;;; guix.scm -- Guix package definition

(use-modules
 (guix packages)
 (guix git-download)
 (guix gexp)
 (guix build-system gnu)
 ((guix licenses) #:prefix license:)
 (guix build-system emacs)
 (gnu packages emacs-xyz)
 (gnu packages screen)
 (ice-9 popen)
 (ice-9 rdelim))

(define %source-dir (dirname (current-filename)))

(define %git-commit
  (read-string (open-pipe "git show HEAD | head -1 | cut -d ' ' -f2" OPEN_READ)))

(define-public emacs-elbazel
  (let ((commit "ea5f35f708fdbc7b4716432fd4be20a3281f5710")
        (revision "0"))
    (package
     (name "emacs-elbazel")
     (version (git-version "0.1" revision commit))
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/niklaseklund/elbazel")
             (commit commit)))
       (sha256
        (base32
         "1d3w7jczh84xi70b8yy1al41jrnqj1pb4fc64cxbprjlmnygaa0n"))
       (file-name (git-file-name name version))))
     (build-system emacs-build-system)
     (propagated-inputs
      `(("emacs-transient" ,emacs-transient)))
     (native-inputs
      `(("emacs-ert-runner" ,emacs-ert-runner)))
     (arguments
      `(#:tests? #t
        #:test-command '("ert-runner")))
     (home-page "https://gitlab.com/niklaseklund/elbazel")
     (synopsis "A Bazel dispatcher for Emacs")
     (description
      "A Bazel dispatcher for Emacs.")
     (license license:gpl3+))))

(package
  (inherit emacs-elbazel)
  (name "emacs-elbazel-git")
  (version (git-version (package-version emacs-elbazel) "HEAD" %git-commit))
  (source (local-file %source-dir
                      #:recursive? #t)))
