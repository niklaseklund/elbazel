;;; elbazel-test.el --- Tests for elbazel.el -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Niklas Eklund

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;; Require

(require 'ert)
(require 'elbazel)

;;; Tests

(ert-deftest elbazel-test-package-targetnames ()
  (cl-letf* (((symbol-function 'elbazel-project-root) (lambda () "/home/user/git"))
             ((symbol-function 'elbazel-search)
              (cl-function
               (lambda (_regexp &key glob)
                 '(("gnu/emacs" "emacs26") ("gnu/emacs" "emacs27"))))))
    (let* ((target "gnu/emacs"))
      (should (equal (elbazel-package-targetnames target)
                     '("emacs26" "emacs27"))))))

(ert-deftest elbazel-test-buildfile ()
  (should (string= (elbazel-buildfile "//foo:bar") "foo/BUILD.bazel"))
  (should (string= (elbazel-buildfile "//bar/...") "bar/BUILD.bazel")))

(ert-deftest elbazel-test-package ()
  (should (string= (elbazel-package "//bar/...") "bar"))
  (should (string= (elbazel-package "//bar") "bar"))
  (should (string= (elbazel-package "//foo:bar") "foo"))
  (should (string= (elbazel-package "//foo/bar:baz") "foo/bar")))

(ert-deftest elbazel-test-target ()
  (should (string= (elbazel-target "//foo:bar") "bar"))
  (should (not (elbazel-target "//bar")))
  (should (not (elbazel-target "//bar/..."))))

(ert-deftest elbazel-test-label ()
  (should (string= (elbazel-label "foo" "bar") "//foo:bar"))
  (should (string= (elbazel-label "bar" 'all) "//bar/..."))
  (should (string= (elbazel-label "bar") "//bar")))

(ert-deftest elbazel-test-test-p ()
  (should (not (elbazel--test-p "//foo/bar")))
  (should (elbazel--test-p "//foo/bar/test:baz"))
  (should (elbazel--test-p "//foo/bar/test/..."))
  (should (elbazel--test-p "//foo/bar/test"))
  (should (elbazel--test-p "//foo/bar/unittests/..."))
  (should (elbazel--test-p "//foo/bar/unittests")))

(ert-deftest elbazel-test-packages-dependent-on-file ()
  (cl-letf* (((symbol-function 'elbazel-project-root) (lambda () "/home/user/git"))
             ((symbol-function 'elbazel-search)
              (cl-function
               (lambda (_regexp &key glob)
                 '(("lisp/libraries" "foo.cpp")
                   ("lisp/test" "bar.cpp")
                   ("lisp/source" "foo.cpp"))))))
    (let* ((file "/home/user/git/lisp/source/foo.cpp"))
      (should (equal (elbazel-packages-dependent-on-file file)
                     '("lisp/source"))))))

(ert-deftest elbazel-test-test-package-dependent-on-package ()
  ;; This function is hard to test fully  since it is recursive
  (cl-letf* (((symbol-function 'elbazel-search)
              (cl-function
               (lambda (_regexp &key glob)
                 '(("git/libraries/foo/test" "//git/libraries/bar/test:bar_support")
                   ("git/libraries/bar/test" "//git/libraries/bar")
                   ("git/targets/foo" "//git/libraries/bar"))))))
    (let ((package "git/libraries/bar"))
      (should (equal '("git/libraries/bar/test")
                     (elbazel-test-packages-dependent-on-package package))))))

(ert-deftest elbazel-test-unbox-sandbox-paths ()
  ;; Case 1
  (cl-letf (((symbol-function 'elbazel-project-root) (lambda () "/home/user/git/project"))
            (elbazel-cache-dir "/home/user/.cache/bazel")
            (content "/home/user/.cache/bazel/_bazel_user/67345e4b985f10023c82503fb0d20213/sandbox/processwrapper-sandbox/466/execroot/project/foo/bar/baz.h:18:63: error: An Error."))
    (should
     (string= "foo/bar/baz.h:18:63: error: An Error."
              (with-temp-buffer
                (insert content)
                (elbazel-unbox-sandbox-paths)
                (buffer-string)))))
  ;; Case 2
  (cl-letf (((symbol-function 'elbazel-project-root) (lambda () "/home/user/git/project"))
            (elbazel-cache-dir "/home/user/.cache/bazel")
            (content "/home/user/.cache/bazel/_bazel_user/67345e4b985f10023c82503fb0d20213/sandbox/processwrapper-sandbox/376/execroot/project/bazel-out/k8-fastbuild/bin/foo/bar/test_baz.runfiles/project/foo/bar/test_baz.py, line 4"))
    (should
     (string= "foo/bar/test_baz.py, line 4"
              (with-temp-buffer
                (insert content)
                (elbazel-unbox-sandbox-paths)
                (buffer-string))))))

(ert-deftest elbazel-test-search-parse ()
  (let ((elbazel--search-regexp (elbazel--search-regexp))
        (rg-result '("./BUILD.bazel:    name = \"foo_py\","
                     "./git/commands/BUILD.bazel:        \"//:foo_py\","
                     "./git/commands/BUILD.bazel:        //:foo_py,"))
        (git-grep-result '("BUILD.bazel:    name = \"foo_py\","
                           "git/commands/BUILD.bazel:        \"//:foo_py\","
                           "git/commands/BUILD.bazel:        //:foo_py,")))
    (should (equal '("" "foo_py") (elbazel--search-parse (elt git-grep-result 0))))
    (should (equal '("" "foo_py") (elbazel--search-parse (elt rg-result 0))))
    (should (equal '("git/commands" "//:foo_py") (elbazel--search-parse (elt git-grep-result 1))))
    (should (equal '("git/commands" "//:foo_py") (elbazel--search-parse (elt rg-result 1))))
    (should (not (elbazel--search-parse (elt rg-result 2))))
    (should (not (elbazel--search-parse (elt git-grep-result 2))))))

;;;; Footer

(provide 'elbazel-test)

;;; elbazel-test.el ends here
