;;; elbazel.el --- A Bazel dispatcher -*- lexical-binding: t -*-

;; Copyright (C) 2021 Niklas Eklund

;; Author: Niklas Eklund <niklas.eklund@posteo.net>
;; URL: https://www.gitlab.com/niklaseklund/elbazel
;; Version: 0.1
;; Package-Requires: ((emacs "27.1"))
;; Keywords: processes productivity

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This package gives the user a quick and easy interface to the Bazel
;; command line interface.

;;;;; External dependencies

;; An optional dependency is ripgrep[1].  Which elbazel uses to grep
;; for information.  If ripgrep is not available it falls back to git
;; grep.
;;
;; [1] https://github.com/BurntSushi/ripgrep

;;;; Credits

;; This package would not have been possible without the following
;; packages: transient[1], which gives the package its Magit like
;; interface.
;;
;;  [1] https://github.com/magit/transient

;;; Code:

;;;; Requirements

(require 'compile)
(require 'subr-x)
(require 'transient)

;;;; Variables

(defvar elbazel-command "bazel" "Bazel command.")
(defvar elbazel-cache-dir (expand-file-name "~/.cache/bazel") "Bazel cache directory.")
(defvar elbazel-buildfile-name "BUILD.bazel" "Name of a buildfile.")
(defvar elbazel-search-with-rg nil "Use `ripgrep' for as search backend.")

(defvar elbazel-history nil "History for elbazel.")
(defvar elbazel-build-history nil "History for build command.")
(defvar elbazel-test-history nil "History for test command.")
(defvar elbazel-edit-history nil "History for the edit command.")

(defvar elbazel--last-build-command nil "The last build command.")
(defvar elbazel--last-test-command nil "The last test command.")
(defvar elbazel--last-edit-command nil "The last edit command.")

;;;;; Private

(defvar elbazel--search-backend nil "Backend to use for searching.")
(defvar elbazel--search-regexp nil "Regexp to parse searches.")

;;;; Transient

;;;;; Top

(transient-define-prefix elbazel-dispatch ()
  "Show popup for running Bazel commands."
  ["Transient commands"
   [("b" "Build" elbazel-build-dispatch)
    ("e" "Edit" elbazel-edit-target)
    ("t" "Test" elbazel-test-dispatch)]])

;;;;; Build

(transient-define-prefix elbazel-build-dispatch ()
  "Show popup for running Bazel build."
  ["Output"
   [("--vf" "failures" "--verbose_failures")
    ("--sd" "debug" "--sandbox_debug")]]
  ["Compilation"
   [(elbazel:-compilation-mode)]]
  ["Remote cache"
   [(elbazel:-remote-cache)
    (elbazel:-remote-download)]]
  ["Commands"
   [("b" "Build" elbazel-build-target)
    ("f" "File dwim" elbazel-build-dwim)
    ("r" "Rerun" elbazel-build-rerun)
    ("y" "Yank" elbazel-build-yank-command)]])

;;;;; Test

(transient-define-argument elbazel-test:-c ()
  :description "compilation mode"
  :class 'transient-option
  :key "-c"
  :argument "--compilation_mode="
  :choices '("fastbuild" "dbg" "opt"))

(transient-define-argument elbazel-test:-o ()
  :description "output"
  :class 'transient-option
  :key "--to"
  :argument "--test_output="
  :choices '("summary" "errors" "all" "streamed"))

(transient-define-prefix elbazel-test-dispatch ()
  "Show popup for running Bazel test."
  :value '("--verbose_test_summary")
  ["Output"
   [("-v" "verbose" "--verbose_test_summary")
    (elbazel-test:-o)]]
  ["Compilation"
   [(elbazel:-compilation-mode)]]
  ["Remote cache"
   [(elbazel:-remote-cache)]]
  ["Test"
   [("f" "File dwim" elbazel-test-dwim)
    ("r" "Rerun" elbazel-test-rerun)
    ("y" "Yank" elbazel-test-yank-command)
    ("t" "Test" elbazel-test-target)]])

;;;;; Common

(transient-define-argument elbazel:-compilation-mode ()
  :description "compilation mode"
  :class 'transient-option
  :key "-c"
  :argument "--compilation_mode="
  :choices '("fastbuild" "dbg" "opt"))

(transient-define-argument elbazel:-remote-cache ()
  :description "remote cache"
  :class 'transient-option
  :key "--rc"
  :allow-empty t
  :argument "--remote_cache=")

(transient-define-argument elbazel:-remote-download ()
  :description "remote download"
  :class 'transient-option
  :key "--rd"
  :allow-empty t
  :argument "--remote_download_outputs="
  :choices '("all"))

;;;; Functions

(defun elbazel-package-targetnames (package)
  "Return all targets in PACKAGE."
  (let ((buildfile (elbazel-buildfile
                    (elbazel-label package))))
    (seq-map (lambda (it)
               (cl-destructuring-bind (_ target) it
                 target))
             (elbazel-search "name =" :glob buildfile))))

(defun elbazel-jump-to-location (label)
  "Jump to LABEL."
  (let* ((buildfile (elbazel-buildfile label))
         (target (elbazel-target label)))
    (find-file buildfile)
    (when target
      (let* ((regexp (format "name = \"%s\"" target)))
        (goto-char (point-min))
        (re-search-forward regexp nil t)))))

(defun elbazel-select-label (labels)
  "Select a label from LABELS."
  (pcase labels
    ((and (pred stringp) label)
     (progn (add-to-history elbazel-history label) label))
    (`(,label)
     (progn (add-to-history elbazel-history label) label))
    (_ (completing-read "Select a target: " labels nil t nil elbazel-history))))

(defun elbazel-select-multiple-labels (labels)
  "Select multiple labels from LABELS."
  (pcase labels
    ((and (pred stringp) label)
     (progn (add-to-history elbazel-history label) label))
    (`(,label)
     (progn (add-to-history elbazel-history label) label))
    (_ (completing-read-multiple "Select one or multiple targets: "
                                    labels
                                    nil
                                    t
                                    nil
                                    elbazel-history))))

(defun elbazel-label (package &optional target)
  "Return a label based on PACKAGE and TARGET."
  (pcase target
    ((and (pred stringp) target)
     (string-join
      `("//" ,package ":" ,target)))
    ('all
     (string-join
      `("//" ,package "/...")))
    (_ (concat "//" package))))

(defun elbazel-buildfile (label)
  "Return LABEL's buildfile."
  (let* ((package (elbazel-package label)))
    (concat (file-name-as-directory package) elbazel-buildfile-name)))

(defun elbazel-package (label)
  "Return package from LABEL."
  (let ((regexp
         (rx "//"
             (group (regexp ".*?"))
             (or ":" "/\.\.\." (regexp "$")))))
    (save-match-data
      (string-match regexp label)
      (match-string 1 label))))

(defun elbazel-target (label)
  "Return target from LABEL."
  (thread-first label
    (split-string ":")
    (seq-elt 1)))

(defun elbazel-labels (&optional glob)
  "Return a list project labels optionally matching GLOB."
  (let* ((target-labels
          (seq-map (lambda (input)
                     (cl-destructuring-bind (package target) input
                       (elbazel-label package target)))
                   (elbazel-search "name =" :glob glob)))
         (package-labels
          (thread-last target-labels
            (seq-map #'elbazel-package)
            (seq-uniq)
            (seq-map (lambda (it) (elbazel-label it 'all))))))
    (cl-concatenate 'list package-labels target-labels)))

(cl-defun elbazel-search (regexp &key glob)
  "Search for REGEXP optionally with GLOB."
  (let* ((elbazel--search-backend (elbazel--search-backend))
         (elbazel--search-regexp (elbazel--search-regexp))
         (results
          (pcase elbazel--search-backend
            ('ripgrep (elbazel--rg regexp glob))
            ('git-grep (elbazel--git-grep regexp glob)))))
    (thread-last results
      (seq-map #'elbazel--search-parse)
      (seq-remove #'null))))

(defun elbazel-packages-dependent-on-file (file)
  "Return all packages that explicitly depend on FILE."
  (pcase-let* ((default-directory (elbazel-project-root))
               (file-relative-project (file-relative-name file default-directory))
               (search-results (elbazel-search (file-name-nondirectory file))))
    (thread-last search-results
      ;; Filter out false positives
      (seq-filter (lambda (input)
                    (cl-destructuring-bind (packagename dependency) input
                      (string-equal file-relative-project
                                    (concat (file-name-as-directory packagename) dependency)))))
      ;; Extract package
      (seq-map (lambda (it)
                 (cl-destructuring-bind (package _) it package)))
      (seq-uniq))))

(defun elbazel-test-packages-dependent-on-package (package)
  "Return a list of all test packages that explicitly depend on PACKAGE."
  (let ((label (elbazel-label package)))
    (if (elbazel--test-p label)
        `(,package)
      (when-let ((labels
                  (thread-last (elbazel-search label)
                    ;; Filter out false positives
                    (seq-filter (lambda (it)
                                  (cl-destructuring-bind (_ dependency) it
                                    (string-equal label dependency))))
                    ;; Extract package
                    (seq-map (lambda (it)
                               (cl-destructuring-bind (package _) it
                                 package)))
                    (seq-uniq)
                    (seq-map (lambda (it) (elbazel-label it))))))
        (if-let ((test-packages
                  (thread-last labels
                    (seq-filter #'elbazel--test-p)
                    (seq-map #'elbazel-package))))
            test-packages
          (thread-last labels
            (seq-remove #'elbazel--test-p)
            (seq-map #'elbazel-package)
            (seq-map #'elbazel-test-packages-dependent-on-package)
            (flatten-list)
            (seq-remove #'null)
            (seq-uniq)))))))

(defun elbazel-compile (command name)
  "Run compilation with COMMAND and print the output in buffer based on NAME."
  (let ((compilation-buffer (lambda (_arg) (format "*elbazel-%s*" name))))
    (apply #'compilation-start `(,command compilation-mode ,compilation-buffer))))

(defun elbazel-unbox-sandbox-paths ()
  "Unbox paths originating from Bazel sandbox."
  (with-connection-local-variables
   (let ((re
          (rx-to-string
           `(seq
             ,elbazel-cache-dir
             (regexp ".*")
             (regexp ,(format "\/execroot.*\/%s\/" (file-name-nondirectory (directory-file-name (elbazel-project-root)))))))))
     (goto-char (point-min))
     (while (search-forward-regexp re nil t)
       (replace-match "")))))

(defun elbazel-unbox-sandbox-paths-h (buffer _string)
  "Unbox paths in BUFFER when STRING match `elbazel-command'."
  (let ((default-directory (elbazel-project-root)))
    (with-current-buffer buffer
      (with-connection-local-variables
       (when (string-prefix-p elbazel-command (car compilation-arguments))
         (let ((buffer-read-only nil))
           (elbazel-unbox-sandbox-paths)))))))

(defun elbazel-project-root ()
  "Return project root."
  (cdr (project-current t)))

;;;; Commands

;;;;; Build

(defun elbazel-build-target (args)
  "Select a target to build with ARGS."
  (interactive
   (list
    (transient-args 'elbazel-build-dispatch)))
  (let* ((default-directory (elbazel-project-root))
         (command (elbazel--build-command
                   #'elbazel-labels
                   args)))
    (setq elbazel--last-build-command command)
    (elbazel-compile command "build")))

(defun elbazel-build-yank-command (args)
  "Yank command for build with ARGS."
  (interactive
   (list
    (transient-args 'elbazel-build-dispatch)))
  (let* ((default-directory (elbazel-project-root))
         (command
          (elbazel--build-command
           #'elbazel-labels
           args)))
    (kill-new command)))

(defun elbazel-build-dwim (args)
  "Build a package for current file with ARGS."
  (interactive
   (list
    (transient-args 'elbazel-build-dispatch)))
  (if-let* ((default-directory (elbazel-project-root))
            (filename (buffer-file-name (current-buffer))))
      (if-let* ((command
                 (elbazel--build-command
                  (lambda ()
                    (seq-map (lambda (it) (elbazel-label it 'all))
                          (elbazel-packages-dependent-on-file filename)))
                  args)))
          (progn
            (setq elbazel--last-build-command command)
            (elbazel-compile command "build"))
        (message "Current file is not found in any package."))
    (message "Current buffer is not associated to a file.")))

(defun elbazel-build-rerun (args)
  "Rerun latest build command with ARGS."
  (interactive
   (list
    (transient-args 'elbazel-build-dispatch)))
  (let* ((default-directory (elbazel-project-root)))
    (if elbazel--last-build-command
        (elbazel-compile elbazel--last-build-command "build")
      (elbazel-build-target args))))

;;;;; Test

(defun elbazel-test-target (args)
  "Test Bazel target with ARGS."
  (interactive
   (list
    (transient-args 'elbazel-test-dispatch)))
  (let* ((default-directory (elbazel-project-root))
         (command (elbazel--test-command
                   (lambda ()
                     (seq-filter #'elbazel--test-p
                                 (elbazel-labels)))
                   args)))
    (setq elbazel--last-test-command command)
    (elbazel-compile command "test")))

(defun elbazel-test-yank-command (args)
  "Yank command for test with ARGS."
  (interactive
   (list
    (transient-args 'elbazel-test-dispatch)))
  (let* ((default-directory (elbazel-project-root))
         (command (elbazel--test-command
                   (lambda ()
                     (seq-filter #'elbazel--test-p
                                 (elbazel-labels)))
                   args)))
    (kill-new command)))

(defun elbazel-test-rerun (args)
  "Rerun latest test command with ARGS."
  (interactive
   (list
    (transient-args 'elbazel-test-dispatch)))
  (let* ((default-directory (elbazel-project-root)))
    (if elbazel--last-test-command
        (elbazel-compile elbazel--last-test-command "test")
      (elbazel-test-target args))))

(defun elbazel-test-dwim (args)
  "Test package for current file with ARGS."
  (interactive
   (list
    (transient-args 'elbazel-test-dispatch)))
  (if-let ((default-directory (elbazel-project-root))
           (filename (buffer-file-name (current-buffer))))
      (if-let ((packages (elbazel-packages-dependent-on-file filename)))
          (if-let ((command
                    (elbazel--test-command
                     (lambda ()
                       (thread-last packages
                         (seq-map #'elbazel-test-packages-dependent-on-package)
                         (flatten-list)
                         (seq-remove #'null)
                         (seq-map (lambda (it) (elbazel-label it 'all)))))
                     args)))
              (progn
                (setq elbazel--last-test-command command)
                (elbazel-compile command "test"))
            (message "Current file is not found in any test package."))
        (message "Current file is not found in any package."))
    (message "Current buffer is not associated to a file.")))

;;;;; Edit

(defun elbazel-edit-target ()
  "Open a target for edit."
  (interactive)
  (let* ((default-directory (elbazel-project-root))
         (elbazel-history 'elbazel-edit-history))
    (elbazel-jump-to-location
     (elbazel-select-label (elbazel-labels)))))

;;;; Support functions

(defun elbazel--command (labels-function select-function command-function)
  "Select label from by applying LABELS-FUNCTION SELECT-FUNCTION and COMMAND-FUNCTION."
  (with-connection-local-variables
   (when-let* ((labels (funcall labels-function))
               (selected
                (funcall select-function labels)))
     (funcall command-function selected))))

;;;;; Build

(defun elbazel--build-command (labels-function args)
  "Return build command for LABELS-FUNCTION with ARGS."
  (let ((elbazel-history 'elbazel-build-history))
    (elbazel--command
     labels-function
     #'elbazel-select-label
     (lambda (label)
       (string-join `(,elbazel-command "build" ,label ,@args) " ")))))

;;;;; Test

(defun elbazel--test-command (labels-function args)
  "Return test command for LABELS-FUNCTION with ARGS."
(let ((elbazel-history 'elbazel-test-history))
  (elbazel--command
   labels-function
   #'elbazel-select-label
   (lambda (label)
     (string-join `(,elbazel-command "test" ,label ,@args) " ")))))

;;;;; Predicates

(defun elbazel--test-p (label)
  "Return t if LABEL is a test."
  (let ((regexp (rx
                 "//"
                 (regexp ".*?")
                 (or ":test" (regexp "tests?/\.\.\.") (regexp "tests?$") (regexp "/test:.*")))))
    (string-match-p regexp label)))

;;;;; Search

(defun elbazel--search-backend ()
  "Return backend to use when searching."
  (if elbazel-search-with-rg
      'ripgrep
    'git-grep))

(defun elbazel--search-regexp ()
  "Return regexp for parsing search."
  (rx-to-string
   `(seq
     (* "./")
     (group (regexp ".*?"))
     (* "/")
     ,elbazel-buildfile-name ":"
     (regexp ".*?")
     "\"" (group (regexp ".*")) "\"")))

(defun elbazel--search-parse (str)
  "Parse STR and return a list."
  (when (string-match elbazel--search-regexp str)
    (list (match-string 1 str)
          (match-string 2 str))))

;;;;; Gitgrep

(defun elbazel--git-grep (regexp &optional glob)
  "Run git-grep with REGEXP with GLOB."
  (let* ((glob (format "*%s" (or glob elbazel-buildfile-name)))
         (args `("grep" "--extended-regexp" ,regexp "--" ,glob)))
    (with-temp-buffer
      (apply #'process-file `("git" nil t nil ,@args))
      (split-string (buffer-string) "\n" t))))

;;;;; Ripgrep

(defun elbazel--rg (regexp &optional glob)
  "Run ripgrep with REGEXP and optional GLOB."
  (let* ((glob (format "--glob=%s" (or glob elbazel-buildfile-name)))
         (args `("--no-heading" ,glob "." "--regexp" ,regexp)))
    (with-temp-buffer
      (apply #'process-file `("rg" nil t nil ,@args))
      (split-string (buffer-string) "\n" t))))

;;;; footer

(provide 'elbazel)

;;; elbazel.el ends here
